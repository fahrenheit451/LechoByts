<!DOCTYPE html>
	<html>
		<head>
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<title>En route pour la fête !</title>
			<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
			<!--<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
			<link rel="icon" type="image/png" href="img/logo.png" />

			<link rel="stylesheet" href="css/style.css">
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
			<link rel="stylesheet" type="text/css" href="harlem/css/animate.css" />

			<script type="text/javascript" src="js/jquery.js"></script>
			<script src="js/jquery.js"></script>
			<script type="text/javascript" src="js/main.js"></script>
			<script type="text/javascript" src="harlem/js/main.js"></script>
		</head>
		<body>
			<header>
				<nav class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li class="navbar-header">
							<a class="navbar-brand" href="?r=site">
								<img class="brand" alt="Acceuil" src="./img/logo.png">
							</a>
						</li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="?r=video">Les gestes qui sauvent</a>
						</li>
						<li>
							<a href="?r=information">Informations</a>
						</li>
						<li>
							<a href="?r=prevision">Prévisions</a>
						</li>
						<li>
							<a href="?r=jeuxdeprevention">Mini jeux</a>
						</li>
						<li>
							<a href="?r=faq">F.A.Q</a>
						</li>
						<li>
							<a id="dapp" href="https://play.google.com/store/apps/details?id=com.lechobyte.enroutepourlafete" target="_blank">Télécharger l'application</a>
						</li>
					</ul>
				</nav>

				<style>
					.parallax1
			        {
			        background-image: url("img/bkg.png");
			        height: 500px;
			        background-attachment: fixed;
			        background-position: center;
			        background-repeat: no-repeat;
			        background-size: cover;
			        }


				</style>
			</header>
