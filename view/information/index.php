<h2>L'APPG</h2>
<p>L’association « APPG » est une association Loi 1901 créée le 12 avril 2006 à l’initiative de CédricDelage, fonctionnaire de la Police nationale.</br></br>
APPG contribue depuis 11 ans maintenant à sensibiliser un public de tout âge aux risques routiers par le biais d’actions de sensibilisation, de prévention et d’éducation.</br>
Son leitmotiv est la sensibilisation à la sécurité routière avec une philosophie et des supports pédagogiques innovants et percutants.</br></br>
Elle a connu une croissance exponentielle, de par sa politique de développement, du sérieux et du professionnalisme de ses intervenants.</br>
Aujourd’hui l’association propose près de 30 ateliers Sécurité Routière  utilisés en milieu scolaire, auprès de mairies et collectivités et en milieu professionnel. </br>
L’association est agréée par le Ministère de l’Intérieur pour tous les types de stages de sensibilisation (alternatives aux poursuites pénales, substitution aux peines judiciaires, récupération des points du permis, etc.) et les tests psychotechniques. </br>
Elle sensibilise et forme aussi les personnels en milieu professionnel à une conduite plus sûre, écologique et économique.</br></p>
</br></br>
<h2>Quelques règles essentielles de sécurité</h2>
<p>
-Adaptez votre vitesse aux circonstances : lieux traversés, conditions de circulation et conditions climatiques, état de la chaussée, chargement du véhicule, état des pneus.</br></br>
-La distance d’arrêt augmente avec la vitesse. Elle correspond à la distance parcourue pendant le temps de réaction du conducteur ajoutée à la distance de freinage du véhicule. Le temps de réaction varie de 1 à 2 secondes. La distance parcourue pendant ce délai augmente avec la vitesse. La distance de freinage du véhicule dépendra de l’état de la chaussée, mais surtout de la vitesse.</br></br>
-Il est donc primordial de respecter les distances de sécurité. Sur la route, laissez au moins 2 secondes entre vous et le véhicule qui vous précède. Sur autoroute, maintenez une distance au moins égale à deux lignes blanches de la bande d’arrêt d’urgence.</br></br>
-Redoublez de vigilance la nuit. Les feux de croisement n’éclairent qu’à 30 mètres. A 70 km/h, l’obstacle qui surgit dans la zone éclairée est inévitable.</p>