<div class="faq">
<div class="faq-header">Voici les réponses aux questions les plus fréquemment posées.</div>

<div class="faq-c">
  <div class="faq-q"><span class="faq-t">+</span>Où obtenir des informations près de chez moi ?</div>
  <div class="faq-a">
    <p>L’association Prévention Routière dispose d’un réseau implanté sur tout le territoire national. Chaque entité (départementale ou régionale) du réseau est chargée de relayer les campagnes de l’association et d’initier ses propres actions. Notre réseau peut vous aider dans la mise en place d’actions de prévention ou vous fournir informations et conseils.</br>
<b>Pour connaître les coordonnées de nos comités départementaux et régionaux, rendez vous sur notre page &nbsp;<a href="https://www.preventionroutiere.asso.fr/agir-dans-votre-region/">« Agir dans votre région ».</a></b></p>
  </div>
</div>

<div class="faq-c">
  <div class="faq-q"><span class="faq-t">+</span>Comment surveiller sa vitesse ?</div>
  <div class="faq-a">
    <p>La question de l’attention au compteur mérite d’être soulevée car les progrès techniques ont fortement réduit l’impression de vitesse. Afin d’éviter les dépassements involontaires de vitesse,</br><b>l’association Prévention Routière demande aux constructeurs qu’ils équipent les véhicules d’un limitateur volontaire de vitesse. Actuellement, un tiers des véhicules neufs sont équipés d’un limitateur ou d’un régulateur de vitesse.</b></p>
  </div>
</div>

<div class="faq-c">
  <div class="faq-q"><span class="faq-t">+</span>Que faire pour éviter la conduite sous influence d’alcool lors d’une sortie ou d’une fête entre amis ?</div>
  <div class="faq-a">
    <p>La meilleure solution consiste à développer le réflexe de choisir, parmi ses amis, celui qui s’engage à ne pas boire d’alcool lors de la soirée. Et ce n’est pas toujours le même, au fil des soirées !
Le choix du conducteur se fait AVANT de sortir et impérativement AVANT absorption d’alcool. Les quantités d’alcool consommées s’additionnent et s’éliminent lentement (environ 0,15 g/h).</br></br>

Les organisateurs de soirées étudiantes, les responsables de discothèques et de bars peuvent mettre en place l’opération « Capitaine de soirée ».</br></br><a href="https://www.preventionroutiere.asso.fr/2016/03/31/alcool-risque-et-facteur-daccident/">En savoir plus sur l'alcool au volant</a></p>
  </div>
</div>

<div class="faq-c">
  <div class="faq-q"><span class="faq-t">+</span>Où puis-je me procurer un éthylotest fiable ?</div>
  <div class="faq-a">
    <p>Il existe deux types d’appareils de dépistage de l’alcoolémie :</br>
– L’éthylotest chimique ou « ballon », à usage unique, dont l’embout change de couleur lorsque la conduite est interdite (il ne donne pas de mesure),</br>
– L’éthylotest électronique, un peu plus cher mais réutilisable, qui fournit une mesure digitale de la concentration d’alcool, exprimée en mg/l.</br></br>

<b>Les éthylotests sont vendus dans les pharmacies et dans les rayons parapharmacies des grandes surfaces. Dans tous les cas, le choix doit se porter sur un produit homologué et certifié par la marque « NF », gages de fiabilité.</b></br></br>

Les forces de l’ordre utilisent, quant à elles, un éthylomètre étalonné qui mesure, en mg/l, la concentration d’alcool dans l’air expiré.</br></br>

<b>Attention : pour les conducteurs novices, titulaire d’un permis probatoire, comme pour les conducteurs de transport en commun, la conduite est interdite à partir de 0,2 g/l d’alcool dans le sang, ce qui correspond à 0,1 mg/l dans l’air expiré.</b></br>
Pour les autres, la limite est fixée à 0,5 g/l dans le sang, soit 0,25 mg/l dans l’air expiré.</p>
  </div>
</div>

</div>
