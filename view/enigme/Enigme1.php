<br/><br/><br/><p><b>Sauriez vous résoudre cette enigme?</b><br/></p>
	
<p>Un chauffeur de taxi s'engage, un peu pressé, dans une ruelle en sens interdit. Il regarde sans broncher le panneau rouge et continue. </br>
Là il est arrêté par un policier particulièrement sévère. Les deux hommes ne se connaîssent pas et non aucun lien de parenté. </br>
Deux minutes auparavant, ce même policier a d'ailleurs verbalisé un automobiliste qui avait engagé sa voiture dans cette même ruelle ! </br>
Pourtant, après avoir contrôlé ses papiers, le policier zélé laisse le chauffeur de taxi repartir sans la moindre amende ! </br>
</br>
Pourquoi ??
</p>
<p>Votre réponse : </p>
<form action="?r=Enigme/enigme2" method="post">
	<input type="text" name="rep"/>
	<input class="btn" type="submit" name="enigme2" value="Valider"/>
</form>