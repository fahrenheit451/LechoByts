<?php

class ErrorController extends Controller 
{
	protected $message='';
	public function __construct($message) {
		$this->message=$message;
	}

	public function index() 
	{
		$this->render('index',$this->message);
	}
}