<?php
try {	
	$parameters = array();
	if (isset($_POST))
		foreach($_POST as $k=>$v)
			$parameters[$k] = $v;
	if (isset($_GET))
		foreach($_GET as $k=>$v)
			$parameters[$k] = $v;

	function parameters()
	{
		global $parameters;
		return $parameters;
	}

	if (isset(parameters()["r"]))
	{
		$route = parameters()["r"];
		if ("default")
			list($controller, $action) = array("site","error");

		if (strpos($route, "/") === FALSE)
			list($controller, $action) = array($route,"index");
		
		else
			list($controller, $action) = explode("/", $route);

		$controller = ucfirst($controller)."Controller";
		$c = new $controller();
		$c->$action();
	}else{
		$c = new SiteController();
		$c->index();
	}
} catch(Exception $e) {
	$c = new ErrorController($e->getMessage());
	$c->index();
}