$( document ).ready(function() {

	var $ = jQuery.noConflict();
	$(window).scroll(function() {

	    //Calcule la hauteur de la page entière
	    var h = $(document).height();
	    //Détermine la position de la page
	    var s = $(window).scrollTop();
	    //Calcule la hauteur de page visible
	    var w = $(window).height();
	    
	    //Formule mathématique pour calculer p le pourcentage de progression
	    var t = (s / h) * w;
	    var p = Math.ceil((s + t) / h * 110) + 1;

	    //Agrandit la barre de progression en fonction de p
	    $('#barre').width(p + '%');
	    if (s > 150){
	      //Modification CSS si l'utilisauter a scrollé plus de 150px
	      $('#barre').height(5);
	      $('#progression').css('height', '5px');   
	      $('.progression-titre').css('opacity', '1');   
	      $('.progression-titre').css('display', 'block');  
	    }else{
	      //Modification CSS si l'utilisateur a scrollé moins de 150px
	      $('#barre').height(5);  
	      $('#progression').css('height', '5px');
	      $('.progression-titre').css('opacity', '0'); 
	      $('.progression-titre').css('display', 'none');        
	    }
	});

	$('.trafic').width($(window).width());
	$('.trafic').height($(this).width());
	
	$(window).resize(function(){
		$('.trafic').width($(window).width());
		$('.trafic').height($(this).width());
	});
	


function trafic(){
	$('.trafic').width($(window).width());
	$('.trafic').height($(this).width());
}

$(".faq-q").click( function () {
  var container = $(this).parents(".faq-c");
  var answer = container.find(".faq-a");
  var trigger = container.find(".faq-t");
  
  answer.slideToggle(200);
  
  if (trigger.hasClass("faq-o")) {
    trigger.removeClass("faq-o");
  }
  else {
    trigger.addClass("faq-o");
  }
});
'use strict';

var bouche = document.querySelector('.bouche'),
    pupilles = document.querySelectorAll('.pupille');

document.addEventListener('mousemove', rebond(function (e) {
    var x = e.clientX,
        y = e.clientY,
        height = window.innerHeight,
        width = window.innerWidth;

    if (y > height / 1.5) {
        bouche.classList.remove('-closed');
    } else {
        bouche.classList.add('-closed');
    }

    var deltaX = (x - width / 2) / width,
        deltaY = (y - height / 2) / height;

    [].forEach.call(pupilles, function (pupille) {
        pupille.style.transform = '\ntranslateX(' + deltaX * 25 + 'px)\ntranslateY(' + deltaY * 25 + 'px)';
    });
}), 100);

function rebond(func, ms) {
    var callAllowed = true;

    return function () {
        if (!callAllowed) {
            return;
        }

        func.apply(this, arguments);

        callAllowed = false;

        setTimeout(function () {
            callAllowed = true;
        }, ms);
    };
};

});
