# L'Echo Byts

Projet du nuit de l'info.

Les défis qui sont en cours sont :
* Easter Egg
* Konami code
* page 404
* API Restful
* GroopTown

## AIDE

Connexion en SSH sur le serveur

### Linux ( Command Line Instruction )

Récuperer le fichier file_rsa ( clé privé ) dans le dossier KEY

```bash
ssh ubuntu@217.182.86.64 -i file_rsa
```
La passphrase est eratotin

### Windows ( puTTY )

Récuperer le fichier file_rsa.ppk ( clé privé ) dans le dossier KEY

Lancer puTTY, dans la partie Catégorie dérouler l'onglet SSH, et ensuite aller dans Auth.
Charger le fichier file_rsa.ppk.

Retourner dans l'onglet principal, dans le host name rentrer l'ip 217.182.86.64, port 22 ( SSH )
Valider la connexion.

Lors de l'ouverture du terminal, le login qu'il demande est ubuntu, et la passphrase est eratotin.
login : ubuntu
passphrase : eratotin